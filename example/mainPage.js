// console.log("Connected to main JS with scrolling")

// const onscroll = (el, listener) => {
//     el.addEventListener('scroll', listener)
//   
/**
* Template Name: Presento - v3.7.0
* Template URL: https://bootstrapmade.com/presento-bootstrap-corporate-template/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
*/

// let footerOfInfo = document.querySelector(".footer");
// footerOfInfo.setAttribute("visibility","hidden")

const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
}

$(document).ready(function() {
    $(".dropdown-toggle").dropdown();
});

let footerOfInfo = document.querySelector(".footer");
// let backtotop = select('.back-to-top')
let navBar = document.querySelector("nav");

if (footerOfInfo) {
    const toggleBacktotop = () => {
        if (window.scrollY > 100) {
            // footerOfInfo.setAttribute("style","visibility:none");

            footerOfInfo.classList.remove("footer_hidden");
            footerOfInfo.classList.add("footer_showing");
            navBar.classList.add("navBar_small");
            // // console.log("showing")
        } else {
            // footerOfInfo.setAttribute("style","visibility:hidden")
            footerOfInfo.classList.add("footer_hidden");
            footerOfInfo.classList.remove("footer_showing");
            // // console.log("hiding")
           
            navBar.classList.remove("navBar_small");
          
        }
    }
    window.addEventListener('load', toggleBacktotop)
    onscroll(document, toggleBacktotop)
}


let navLoginElem = document.querySelector("#nav-login")
let navLogoutElem = document.querySelector("#nav-logout")
async function fetchUser() {
    let res = await fetch('/user')
    if (res.ok) {
        let result = await res.json();
        if (!result.data) {
            // console.log("role unknown")
            return
          }
        
        document.querySelector('#loginUser').innerHTML = result.data.user.username
        // console.log('result =', result.data.user);
        navLoginElem.setAttribute('hidden', '')
        navLogoutElem.removeAttribute('hidden')
        if(result.data.user.role == 'admin') {
            document.querySelector('#admin').removeAttribute('hidden')
        }
    } else {
        navLogoutElem.setAttribute('hidden', '')
        navLoginElem.removeAttribute('hidden')
        // console.log('not yet login');
    }
    
}



document.querySelector("#logout").addEventListener("click", async (event) => {
   
    let res = await fetch("/logout",{
        method: "POST"
    })
    if (res.ok) {
        navLogoutElem.setAttribute('hidden', '')
        navLoginElem.removeAttribute('hidden')
    }
});


fetchUser()
