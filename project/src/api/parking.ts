const { REACT_APP_API_SERVER } = process.env;

export async function fetchGetPostedParkingItem() {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/parking`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  return res;
}

export async function fetchGetUserById(id: number) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/parking/getUser`, {
    method: "POST",
    body: JSON.stringify({
      id: id,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}
