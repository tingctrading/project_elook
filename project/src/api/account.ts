const { REACT_APP_API_SERVER } = process.env;

export async function fetchGetReportedItem(userId: number) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/users/reported`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      id: userId,
    }),
  });
  return res;
}
