const { REACT_APP_API_SERVER } = process.env;

export async function fetchGetContactListItems() {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/tow`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  return res;
}
