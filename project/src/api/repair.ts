const { REACT_APP_API_SERVER } = process.env;

export async function fetchGetRepairItem() {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/repair`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  return res;
}
