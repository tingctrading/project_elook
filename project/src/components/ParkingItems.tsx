import { useEffect, useState } from "react";
import { Container, Row, Col, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { fetchGetUserById } from "../api/admin";
import {
  getPendingParkingItemThunk,
  getPostedParkingItemThunk,
  removeParkingItemThunk,
} from "../redux/admin/thunk";
import { IRootState } from "../redux/store";
interface ParkingItem {
  id: number;
  place: string;
  location: string;
  hourly_rent: number;
  day_rent: number;
  night_rent: number;
  isIndoor: boolean;
  image: string;
  remark: string;
  users_id: number;
  post: boolean;
}

export default function ParkingItems(props: ParkingItem) {
  const dispatch = useDispatch();
  const removeItem = (id: number) => {
    dispatch(removeParkingItemThunk(id));
    dispatch(getPendingParkingItemThunk());
    dispatch(getPostedParkingItemThunk());
  };
  const [userName, setUserName] = useState("");
  useEffect(() => {
    const fetchData = async () => {
      const result = await fetchGetUserById(props.users_id);
      const data = await result.json();

      const email = data[0].email;
      setUserName(email);
    };
    fetchData();
  }, []);
  return (
    <div className="parkingItemData">
      <Form>
        <Container className="parkingItems">
          <Row key={props.id}>
            <Col>
              <h2>
                {props.id} {props.place}
              </h2>
              <h2> {props.location}</h2>
              <h3>
                時租:${props.hourly_rent} 日租:${props.day_rent} 夜租:$
                {props.night_rent}{" "}
                {props.isIndoor ? "室內停車場" : "露天停車場"}
              </h3>
              <h3>
                由會員{userName}提供資訊 {props.post ? "已上載" : "等待上載"}
              </h3>
              <h3>Remark: {props.remark}</h3>
            </Col>
            <Col className="parkingBtn">
              <div className="btn" onClick={() => removeItem(props.id)}>
                刪除
              </div>
            </Col>
          </Row>
        </Container>
      </Form>
    </div>
  );
}
