import React, { Component, useState } from "react";
import { Modal, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { SetIsInfoModalClose } from "../redux/util/action";
import "../css/modal.css";
export function InfoModal(props: any) {
  const dispatch = useDispatch();
  const handleClose = () => {
    dispatch(SetIsInfoModalClose());
  };
  const { isShow, onHide, content } = props;
  return (
    <Modal
      dialogClassName="custom-dialog"
      size="sm"
      show={isShow}
      onHide={handleClose}
      aria-labelledby="example-modal-sizes-title-sm"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-sm">{content}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <button onClick={handleClose}>關閉</button>
      </Modal.Body>
    </Modal>
  );
}
