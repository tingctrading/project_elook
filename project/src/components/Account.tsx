import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { Accordion, Form } from "react-bootstrap";
import { logout } from "../redux/auth/actions";
import { IRootState } from "../redux/store";
import "../css/account.css";
import { changePasswordThunk } from "../redux/auth/thunks";
import { getReportedItemThunk } from "../redux/account/thunk";
import ReportedItems from "./ReportedItems";
import { useEffect, useState } from "react";
import { InfoModal } from "./InfoModal";
import { SetModalContent } from "../redux/util/action";
import { Button } from "reactstrap";

interface FormState {
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
}

export default function Account() {
  const isAuthenticate = useSelector(
    (state: IRootState) => state.auth.isAuthenticate
  );
  const dispatch = useDispatch();
  const email = useSelector((state: IRootState) => state.auth.user.email);
  let userId = useSelector((state: IRootState) => state.auth.user.id);
  let isInfoModalShow = useSelector(
    (state: IRootState) => state.util.isInfoModalShow
  );
  let content = useSelector((state: IRootState) => state.util.content);
  let id = parseInt(userId);
  const reportedItems = useSelector(
    (state: IRootState) => state.acc.reportedItems
  );

  useEffect(() => {
    dispatch(getReportedItemThunk(id));
  }, []);
  const onSubmit = (data: FormState) => {
    const { currentPassword, newPassword, confirmPassword } = data;
    if (
      currentPassword === "" ||
      newPassword === "" ||
      confirmPassword === ""
    ) {
      dispatch(SetModalContent("請輸入密碼。"));
    } else {
      if (data.newPassword === data.confirmPassword) {
        dispatch(
          changePasswordThunk(email, data.currentPassword, data.newPassword)
        );
        reset();
      } else {
        dispatch(SetModalContent("確認密碼不正確，請重新輸入。"));
        reset();
      }
    }
  };

  const clickLogout = () => {
    dispatch(logout());
  };

  const { register, handleSubmit, getValues, setValue, reset } =
    useForm<FormState>({});

  return (
    <div className="user-container">
      <h1>個人帳戶</h1>
      <div>
        <div>帳戶:{email}</div>
        {/* <Form onSubmit={handleSubmit(onSubmit)}> */}
        <Accordion>
          <Accordion.Item eventKey="1">
            <Accordion.Header className="adminHeader">
              {" "}
              更改密碼
            </Accordion.Header>
            <Accordion.Body>
              <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group>
                  <div className="form-questions">更改密碼</div>
                  <Form.Control
                    className="form-questions"
                    type="password"
                    placeholder="現有密碼："
                    {...register("currentPassword", { maxLength: 20 })}
                  />
                  <Form.Control
                    className="form-questions"
                    type="password"
                    placeholder="新密碼："
                    {...register("newPassword", { maxLength: 20 })}
                  />
                  <Form.Control
                    className="form-questions"
                    type="password"
                    placeholder="確認密碼："
                    {...register("confirmPassword", { maxLength: 20 })}
                  />
                </Form.Group>
                <Button type="submit" variant="light">
                  提交
                </Button>
              </Form>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>

        {/* 
          <Form.Group>
            <div className="form-questions">更改密碼</div>
            <Form.Control
              className="form-questions"
              type="password"
              placeholder="現有密碼："
              {...register("currentPassword", { maxLength: 20 })}
            />
            <Form.Control
              className="form-questions"
              type="password"
              placeholder="新密碼："
              {...register("newPassword", { maxLength: 20 })}
            />
            <Form.Control
              className="form-questions"
              type="password"
              placeholder="確認密碼："
              {...register("confirmPassword", { maxLength: 20 })}
            />
          </Form.Group>
          <input type="submit" className="btn" />
        </Form> */}

        <Accordion defaultActiveKey="0">
          <Accordion.Item eventKey="0">
            <Accordion.Header className="adminHeader">
              {" "}
              你提供的資訊
            </Accordion.Header>
            <Accordion.Body>
              {reportedItems.map((reportedItem, index) => (
                <ReportedItems
                  key={index}
                  id={reportedItem.id}
                  place={reportedItem.place}
                  location={reportedItem.location}
                  hourly_rent={reportedItem.hourly_rent}
                  day_rent={reportedItem.day_rent}
                  night_rent={reportedItem.night_rent}
                  isIndoor={reportedItem.isIndoor}
                  image={reportedItem.image}
                  remark={reportedItem.remark}
                  users_id={reportedItem.users_id}
                  post={reportedItem.post}
                />
              ))}
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>

        <button onClick={clickLogout} className="logoutBtn">
          登出
        </button>
      </div>
      {/* <InfoModal show={isInfoModelShow} onHide={onInfoModalHide} /> */}
    </div>
  );
}
