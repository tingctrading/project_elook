import React from "react";
import MyVideo from "../asset/video/background.mp4";

class MyVideoComponent extends React.Component {
  render() {
    return (
      <video className="background-video" width="100%" height="100%" autoPlay loop muted>
        <source src={MyVideo} type="video/mp4" />
        Your browser does not support HTML5 video.
      </video>
    );
  }
}

export default MyVideoComponent