import { useForm } from "react-hook-form";
import { FormCheckType } from "react-bootstrap/esm/FormCheck";
import "../css/report.css";
import { NumberLiteralType } from "typescript";
import apiKey from "../key";
import { IRootState } from "../redux/store";
import { IAuthState } from "../redux/auth/state";
import { useDispatch, useSelector } from "react-redux";
import { debounce, result } from "lodash";
import { SetMapApi, SetMapApiLoaded } from "../redux/parking/actions";
import HeaderLogo from "./HeaderLogo";

import {
  Row,
  Col,
  Form,
  Button,
  ButtonGroup,
  InputGroup,
} from "react-bootstrap";
import "../css/report.css";
import React, { useEffect, useState } from "react";
import { createReportDataThunk } from "../redux/report/thunks";
import { SetModalContent } from "../redux/util/action";

interface FormState {
  users_id: string;
  place: string;
  location: string;
  hourly_rent: number;
  day_rent: number;
  night_rent: number;
  isIndoor: boolean;
  place_id: string;
  image: string;
  remark: string;
}
interface autocompleteResultsState {
  id: number;
  place_id: string;
  description: string;
}

export default function Report() {
  const dispatch = useDispatch();
  const [autocompleteResults, setAutocompleteResults] = useState<
    autocompleteResultsState[]
  >([]);
  const userId = useSelector((state: IRootState) => state.auth.user.id);

  const mapApi = useSelector((state: IRootState) => state.parking.mapApi);
  const mapApiLoaded = useSelector(
    (state: IRootState) => state.parking.mapApiLoaded
  );

  const handleAutocomplete = () => {
    if (mapApiLoaded) {
      const service = new mapApi.places.AutocompleteService();
      const request = {
        input: getValues("place"),
      };

      service.getPlacePredictions(request, (results: any, status: any) => {
        if (status === mapApi.places.PlacesServiceStatus.OK) {
          setAutocompleteResults(results);
        }
      });
    }
  };

  const { register, handleSubmit, getValues, setValue, reset } =
    useForm<FormState>({
      defaultValues: {
        day_rent: 0,
        night_rent: 0,
        hourly_rent: 0,
      },
    });
  const handleClickToGetPlaceDetails = (e: any) => {
    const placeId = e.target.getAttribute("id");
    const description = e.target.innerText;
    setValue("place", description);
    setValue("place_id", placeId);
  };

  const onSubmit = (data: FormState) => {
    dispatch(createReportDataThunk(data));
    dispatch(SetModalContent("已成功提交"));
    reset();
  };

  return (
    <div>
      <h1>我要報料</h1>
      <div className="report-container">
        <Form onSubmit={handleSubmit(onSubmit)} className="form-container">
          <div className="form-questions">電單車泊車位在：</div>
          <Form.Group>
            <Form.Control
              placeholder="請輸入地址"
              {...register("place", {
                onChange: debounce(handleAutocomplete, 500),
                maxLength: 40,
                required: true,
              })}
            />
            <div
              className="auto_complete_list"
              onClick={handleClickToGetPlaceDetails}
            >
              {autocompleteResults.length !== 0 ? (
                <h2 className="text">請選擇其中以下一個地點:</h2>
              ) : (
                <div></div>
              )}

              {autocompleteResults &&
                autocompleteResults.map((item, id) => (
                  <div key={id} id={item.place_id} className="auto-item">
                    {item.description.substring(2)}
                  </div>
                ))}
            </div>
          </Form.Group>
          <div className="form-questions">價錢：（請填寫適用者）</div>
          <Form.Group as={Row} controlId="formPlaintext">
            <Form.Label column xs={4}>
              時租：
            </Form.Label>
            <Col xs={6}>
              <Form.Control
                className="input"
                type="price"
                {...register("hourly_rent", { maxLength: 3 })}
              />
            </Col>
            <Form.Label column xs={4}>
              日泊：
            </Form.Label>
            <Col xs={6}>
              <Form.Control
                className="input"
                type="price"
                placeholder="日泊"
                {...register("day_rent", { maxLength: 3 })}
              />
            </Col>
            <Form.Label column xs={4}>
              夜泊：
            </Form.Label>
            <Col xs={6}>
              <Form.Control
                className="input"
                type="price"
                placeholder="夜泊"
                {...register("night_rent", { maxLength: 3 })}
              />
            </Col>
          </Form.Group>
          <Form.Group>
            <ButtonGroup aria-label="Indoor" className="indoorbuttongp">
              <Button
                variant="outline-light"
                className="input"
                type="button"
                onClick={() => setValue("isIndoor", true)}
              >
                室內停車場
              </Button>
              <Button
                variant="outline-light"
                className="input"
                type="button"
                onClick={() => setValue("isIndoor", false)}
              >
                室外停車場
              </Button>
            </ButtonGroup>
            <Form.Control
              as="textarea"
              rows={3}
              placeholder="其他資訊，例如停車場入口，泊車優惠等等"
              {...register("remark", { maxLength: 50 })}
            />
          </Form.Group>
          <Button
            type="submit"
            variant="light"
            onClick={() => setValue("users_id", userId)}
          >
            提交
          </Button>
        </Form>
      </div>
    </div>
  );
}
