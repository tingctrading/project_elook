import React, { useEffect, useState } from "react";
import { connectAdvanced, useDispatch, useSelector } from "react-redux";
import { IRootState } from "../redux/store";
import { getContactListThunk } from "../redux/tow/thunks";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhone, faTruckPickup, faCopy} from "@fortawesome/free-solid-svg-icons";
import { Container, Row, Col } from "react-bootstrap";
import "../css/tow.css";
import { SetModalContent } from "../redux/util/action";

export default function ContactList() {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const contactLists = useSelector(
    (state: IRootState) => state.tow.contactLists
  );

  useEffect(() => {
    dispatch(getContactListThunk());
  }, [dispatch]);

  return (
    <div className="outer-container">
      <h1>拖車聯絡資料</h1>
      <div>
        {contactLists.map((contactList) => (
          <div key={contactList.id} className="contactList">
            <Container>
              <Row className="cell">
                <Col xs={2} className="towIcon"><FontAwesomeIcon icon = {faTruckPickup} size="2x"/></Col>
                <Col xs={6}><h2>{contactList.name}</h2>
                 <h3>聯絡電話: {contactList.phone}</h3>
                 <h3>語言: {contactList.language} {contactList.second_language}</h3></Col>
                <Col className="towIcon">
                   <div onClick={() => 
                 {navigator.clipboard.writeText(
                   (contactList.name) + "  聯絡電話:" +(contactList.phone))
                 .then(()=>{dispatch(SetModalContent("已成功複製" +(contactList.name)+"的聯絡資料"))})
                 .catch(()=>{dispatch(SetModalContent("請再嘗試"))})}}><FontAwesomeIcon icon = {faCopy} size="2x" className="fa-icon" /><br></br>
                 <h3>複製</h3></div>
                </Col>
                <Col className="towIcon">
                  <div><a href={'tel:+852'+contactList.phone}>
                  <FontAwesomeIcon icon = {faPhone} flip="horizontal" size="2x" className="fa-icon"/></a><br></br>
                  <h3>致電</h3></div>
                </Col>
              </Row>
            </Container>
          </div>
        ))}
      </div>
    </div>
  );
}


