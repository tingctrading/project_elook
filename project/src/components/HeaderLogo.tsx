import React from "react";
import HLogo from "../asset/image/elook_black.png"
import "../css/headerlogo.css"

class HeaderLogo extends React.Component {
    render() {
      return (
          <img className="hlogo" src={HLogo} alt="logo"/>
      );
    }
  }
  
  export default HeaderLogo;