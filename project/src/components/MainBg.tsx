import React, {useState, useEffect} from "react";
import MyBg from "../asset/image/main_bg.jpg";
import Logo from "../asset/image/elook_white.gif";
import "../css/mainbg.css";


export default function MyBgComponent() {
 
    return (

      <><img className="background-image" src={MyBg} alt="Background" /><img className="mainlogo" src={Logo} alt="logo" /></>
    );
  }
