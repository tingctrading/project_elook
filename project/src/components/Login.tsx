import { push } from "connected-react-router";
import { Form } from "react-bootstrap";
import ReactFacebookLogin, {
  ReactFacebookLoginInfo,
} from "react-facebook-login";
import { GoogleLogin } from "react-google-login";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { GoogleLoginButton } from "ts-react-google-login-component/lib/components/GoogleLogin/GoogleLogin";
import "../css/login.css";
import {
  createUserThunk,
  loginFacebookThunk,
  loginGoogleThunk,
  loginThunk,
} from "../redux/auth/thunks";
import { SetModalContent } from "../redux/util/action";
interface FormState {
  email: string;
  password: string;
}

export default function Login() {
  const dispatch = useDispatch();
  const { register, handleSubmit, getValues, reset } = useForm<FormState>({});

  const onSubmit = (data: FormState) => {
    dispatch(loginThunk(data.email, data.password));
    reset();
  };
  const handleFailure = (result: any) => {};

  const handleLogin = (googleData: any) => {
    let accessToken = googleData.tokenId;

    if (accessToken) {
      dispatch(loginGoogleThunk(accessToken));
    }
  };

  const fBCallback = (
    userInfo: ReactFacebookLoginInfo & { accessToken: string }
  ) => {
    if (userInfo.accessToken) {
      dispatch(loginFacebookThunk(userInfo.accessToken));
    }
  };

  const createNewUser = () => {
    const data = getValues();
    dispatch(createUserThunk(data.email, data.password));
  };

  return (
    <div>
      <div className="login-form-container">
        <Form onSubmit={handleSubmit(onSubmit)} className="login-form">
          <Form.Group>
            <Form.Label className="title">電郵</Form.Label>
            <Form.Control
              className="input"
              type="email"
              {...register("email", {})}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label className="title">密碼</Form.Label>
            <Form.Control
              className="input"
              type="password"
              {...register("password", {})}
            />
          </Form.Group>
          <button type="submit" value="Submit" className="loginBtn ">
            登入
          </button>
        </Form>
        <button onClick={createNewUser} className="loginBtn ">
          註冊
        </button>
        <br />

        <div>或</div>

        <div>
          <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID || ""}
            buttonText="LOGIN WITH GOOGLE"
            onSuccess={handleLogin}
            onFailure={handleFailure}
            cookiePolicy={"single_host_origin"}
          ></GoogleLogin>
          <br />
        </div>

        <ReactFacebookLogin
          appId={process.env.REACT_APP_FACEBOOK_APP_ID || ""}
          autoLoad={false}
          fields="name,email,picture"
          onClick={() => {}}
          callback={fBCallback}
          reAuthenticate={true}
        />
      </div>
    </div>
  );
}
