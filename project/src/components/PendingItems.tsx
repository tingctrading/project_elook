import { Container, Row, Col, Form } from "react-bootstrap";
import { useDispatch } from "react-redux";
import {
  getPendingParkingItemThunk,
  getPostedParkingItemThunk,
  postParkingItemThunk,
} from "../redux/admin/thunk";
import ContentEditable from "react-contenteditable";
import { useRef, useState, useEffect } from "react";
import { editReportedItemThunk } from "../redux/admin/thunk";
import { fetchGetUserById } from "../api/admin";

interface PendingItem {
  id: number;
  place: string;
  location: string;
  hourly_rent: number;
  day_rent: number;
  night_rent: number;
  isIndoor: boolean;
  image: string;
  remark: string;
  users_id: number;
  post: boolean;
}

export default function PendingItems(props: PendingItem) {
  const dispatch = useDispatch();
  const postItem = (id: number) => {
    dispatch(postParkingItemThunk(id));
    dispatch(getPostedParkingItemThunk());
    dispatch(getPendingParkingItemThunk());
  };
  // const address1Ref = useRef("address1");

  const [parkingItem, setParkingItem] = useState({
    place: "",
    location: "",
    hourly_rent: "",
    day_rent: "",
    night_rent: "",
    isIndoor: true,
    image: "",
    remark: "",
  });
  useEffect(() => {
    let parkingForm = JSON.parse(JSON.stringify(props));
    parkingForm.hourly_rent = props.hourly_rent.toString();
    parkingForm.day_rent = props.day_rent.toString();
    parkingForm.night_rent = props.night_rent.toString();
    setParkingItem(parkingForm);
  }, []);

  const handleChange = (event: any, field: string) => {
    setParkingItem({ ...parkingItem, [field]: event.target.value });
  };

  const handleEdit = (id: number) => {
    dispatch(editReportedItemThunk(id, parkingItem));
  };

  const [userName, setUserName] = useState("");
  useEffect(() => {
    const fetchData = async () => {
      const result = await fetchGetUserById(props.users_id);
      const data = await result.json();
      const email = data[0].email;
      setUserName(email);
    };
    fetchData();
  }, []);

  return (
    <div className="dataContainer parkingItemData">
      <Form>
        <Container>
          <Row key={props.id}>
            <div className="itemsContainer">
              <Col className="parkingItems">
                <h2>{props.isIndoor ? "室內停車場" : "露天停車場"}</h2>
                <h2>
                  資訊 {props.id} 由會員{userName}提供{" "}
                </h2>
                <div className="reportData">
                  <span className="subtitle">地點:</span>
                  <ContentEditable
                    className="textbox"
                    suppressContentEditableWarning={true}
                    html={parkingItem.place}
                    onChange={(evt: any) => {
                      handleChange(evt, "place");
                    }}
                  />
                </div>
                <div className="reportData">
                  <span className="subtitle">街道:</span>
                  <ContentEditable
                    className="textbox"
                    suppressContentEditableWarning={true}
                    html={parkingItem.location}
                    onChange={(evt: any) => {
                      handleChange(evt, "location");
                    }}
                  />
                </div>
                <div className="reportData">
                  <span className="subtitle">時租:</span>
                  <ContentEditable
                    className="textbox"
                    suppressContentEditableWarning={true}
                    html={parkingItem.hourly_rent}
                    onChange={(evt: any) => {
                      handleChange(evt, "hourly_rent");
                    }}
                  />
                </div>

                <div className="reportData">
                  <span className="subtitle">日泊:</span>
                  <ContentEditable
                    className="textbox"
                    suppressContentEditableWarning={true}
                    html={parkingItem.day_rent}
                    onChange={(evt: any) => {
                      handleChange(evt, "day_rent");
                    }}
                  />
                </div>

                <div className="reportData">
                  <span className="subtitle">夜泊:</span>
                  <ContentEditable
                    className="textbox"
                    suppressContentEditableWarning={true}
                    html={parkingItem.night_rent}
                    onChange={(evt: any) => {
                      handleChange(evt, "night_rent");
                    }}
                  />
                </div>
                <div className="reportData">
                  <span className="subtitle">備註:</span>
                  <ContentEditable
                    className="textbox"
                    suppressContentEditableWarning={true}
                    html={parkingItem.remark}
                    onChange={(evt: any) => {
                      handleChange(evt, "remark");
                    }}
                  />
                </div>
              </Col>
              <Col className="parkingBtn">
                <div
                  className="btn"
                  onClick={() => {
                    handleEdit(props.id);
                  }}
                >
                  更改
                </div>
                <div className="btn" onClick={() => postItem(props.id)}>
                  發佈
                </div>
              </Col>
            </div>
          </Row>
        </Container>
      </Form>
    </div>
  );
}
