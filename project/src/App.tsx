import React, { Component, useState, useEffect } from "react";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import { Route, Switch, useHistory } from "react-router";
import MyBgComponent from "./components/MainBg";
import Navigation from "./components/Navigation";
import Report from "./components/Report";
import Repair from "./components/Repair";
import Account from "./components/Account";
import ContactList from "./components/Tow";
import Login from "./components/Login";
import { ConnectedRouter } from "connected-react-router";
import { history, IRootState } from "./redux/store";
import PrivateRoute from "./components/PrivateRoute";
import SimpleMap from "./components/SimpleMap";
import Admin from "./components/Admin";
import ReactFacebookLogin, {
  ReactFacebookLoginInfo,
} from "react-facebook-login";
import { loginFacebookThunk } from "./redux/auth/thunks";
import { useDispatch, useSelector } from "react-redux";
import { InfoModal } from "./components/InfoModal";

function App() {
  const [isDisplayed, setIsDisplayed] = useState(false);
  useEffect(() => {
    setInterval(() => {
      setIsDisplayed(true);
    }, 2000);
  }, []);

  const [isHide, setIsHide] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setIsHide(false);
    }, 2000);
  }, []);
  const dispatch = useDispatch();

  const fBCallback = (
    userInfo: ReactFacebookLoginInfo & { accessToken: string }
  ) => {
    if (userInfo.accessToken) {
      dispatch(loginFacebookThunk(userInfo.accessToken));
    }
  };
  let { isInfoModalShow, content } = useSelector(
    (state: IRootState) => state.util
  );

  const onHide = () => {};
  return (
    <div className="App">
      <ConnectedRouter history={history}>
        {isHide && (
          <>
            <MyBgComponent />
          </>
        )}
        {isDisplayed && (
          <>
            <InfoModal
              isShow={isInfoModalShow}
              onHide={onHide}
              content={content}
            />
            <Navigation />
            <Switch>
              <PrivateRoute path="/report" component={Report} />
              <Route path="/parking" component={SimpleMap} />
              <Route path="/tow" component={ContactList} />
              <Route path="/login" component={Login} />
              <PrivateRoute path="/account" component={Account} />
              <PrivateRoute path="/admin" component={Admin} />
              <Route path="/" component={SimpleMap} />
            </Switch>
          </>
        )}
      </ConnectedRouter>
      <div className="fbBtnContainer">
        <ReactFacebookLogin
          appId={process.env.REACT_APP_FACEBOOK_APP_ID || ""}
          autoLoad={false}
          fields="name,email,picture"
          onClick={() => {}}
          callback={fBCallback}
          reAuthenticate={true}
        />
      </div>
    </div>
  );
}

export default App;
