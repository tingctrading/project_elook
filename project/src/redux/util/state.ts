export interface IUtilState {
  isInfoModalShow: boolean;
  content: string;
}
