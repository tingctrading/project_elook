import { Dispatch } from "redux";
import { failed, getContactList, IContactListAction } from './actions'
import { fetchGetContactListItems } from "../../api/tow";
import { ContactListState } from "./state";

export function getContactListThunk() {
    return async (dispatch: Dispatch<IContactListAction>) => {
        const res = await fetchGetContactListItems()
        const result: any = await res.json()
        const items: ContactListState[] = result.items
        
        if (res.ok) {
            dispatch(getContactList(items))
        } else {
            dispatch(failed("@@Tow/GOT_CONTACT_LIST_FAILED", "fail getContactListThunk"))
        }
    }
}

