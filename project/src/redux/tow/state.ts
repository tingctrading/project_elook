export interface ContactListState {
    id: number;
    name: string;
    phone: number;
    language: string;
    second_language: string;
    status: string;
}

export interface IContactListState {
    contactLists: ContactListState[]
}