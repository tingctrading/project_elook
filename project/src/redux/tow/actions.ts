import { IContactListState, ContactListState } from "./state"

export function getContactList(data: ContactListState[]) {
    return {
        type: "@@Tow/LOAD_CONTACT_LIST" as const,
        data
    }
}

type FAILED_INTENT =
    | "@@Tow/GOT_CONTACT_LIST_FAILED"


export function failed(type: FAILED_INTENT, msg: string) {
    return {
        type,
        msg
    }
}

export type IContactListAction =
    | ReturnType<typeof getContactList>
    | ReturnType<typeof failed>