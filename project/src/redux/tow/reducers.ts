import { IContactListAction } from "./actions";
import { IContactListState, ContactListState } from "./state";

const initialState: IContactListState = {
    contactLists: []

}

export const contactListReducers = (state: IContactListState = initialState, action: IContactListAction): any => {
    switch (action.type) {
        case '@@Tow/LOAD_CONTACT_LIST':
            const newContactListItems: ContactListState[] = [...action.data]
            return {
                ...state,
                contactLists: newContactListItems,
            }

        case '@@Tow/GOT_CONTACT_LIST_FAILED':
            return state
        default:
            return state
    }
}