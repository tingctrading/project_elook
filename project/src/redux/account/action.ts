import { ReportedItemState } from "./state"




export function test() {
    return {
        type: "TEST" as const
    }
}

export function GotReportedItems(data: ReportedItemState[]) {
    return {
        type: '@@Acc/LOAD_REPORTED_PARKING_ITEMS' as const,
        data
    }
}


type FAILED_INTENT =
    | "@@Acc/GOT_REPORTED_ITEMS_FAILED"


export function failed(type: FAILED_INTENT, msg: string) {
    return {
        type,
        msg
    }
}




export type IAccAction =
    | ReturnType<typeof test>
    | ReturnType<typeof GotReportedItems>
    | ReturnType<typeof failed>