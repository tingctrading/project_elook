import { IAccAction } from "./action"
import { IAccState, ReportedItemState } from "./state"

const initialState: IAccState = {
    reportedItems: [],

}
export const accountReducers = (state: IAccState = initialState, action: IAccAction): IAccState => {
    switch (action.type) {

        case '@@Acc/LOAD_REPORTED_PARKING_ITEMS':
            const newReportedItems: ReportedItemState[] = [...action.data]
            return {
                ...state,
                reportedItems: newReportedItems,
            }

        default:
            return state
    }
}