export interface ReportState {
  location: string;
  hourly_rent: number;
  day_rent: number;
  night_rent: number;
  isIndoor: boolean;
  image: string;
  remark: string;
}

export interface IReportState {
  reportData: ReportState[];
  createReport: ReportState[];
}
