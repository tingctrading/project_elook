import { Dispatch } from "redux";
import {
  failed,
  getReportData,
  IReportAction,
  createReportData,
} from "./actions";
import { fetchGetReportData, fetchCreateReportData } from "../../api/report";
import { ReportState } from "./state";

export function getReportDataThunk() {
  return async (dispatch: Dispatch<IReportAction>) => {
    const res = await fetchGetReportData();
    const result: any = await res.json();
    const items: ReportState[] = result.items;
    if (res.ok) {
      dispatch(getReportData(items));
    } else {
      dispatch(
        failed("@@Report/GOT_REPORT_DATA_FAILED", "fail getReportDataThunk")
      );
    }
  };
}

export function createReportDataThunk(data: ReportState) {
  return async (dispatch: Dispatch<IReportAction>) => {
    const res = await fetchCreateReportData(data);
    const result = await res.json();
    if (res.ok) {
      dispatch(createReportData(result));
    } else {
      alert("此地點已被爆料!");
    }
  };
}
