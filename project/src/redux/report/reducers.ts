import { IReportAction } from "./actions";
import jwt from 'jwt-decode';
import { IReportState, ReportState } from "./state";
import { loadToken } from "../auth/actions";

const initialState: IReportState = {
    reportData: [],
    createReport: [],
}

export const reportDataReducers = (state: IReportState = initialState, action: IReportAction): any => {
    switch (action.type) {
        case '@@Report/LOAD_REPORT_DATA':
            const newReportData: ReportState[] = [...action.data]
            return {
                ...state,
                reportData: newReportData,
            }

        case '@@Report/CREATE_REPORT_DATA':
            const createReportData: ReportState[] = { ...action.data }
            return {
                ...state,
                createReport: createReportData,

            }

        default:
            return state
    }
}

