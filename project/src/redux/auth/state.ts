export interface IAuthState {
    isAuthenticate: boolean
    msg: string
    user: JWTPayload
    isAdmin: boolean
}

export interface JWTPayload {
    email: string
    role: string
    id: string

}