import { IAuthState } from "./state";
import { IAuthAction } from "./actions";
import jwt from "jwt-decode";
import { JWTPayload } from "./state";

const initialState: IAuthState = {
  isAuthenticate: localStorage.getItem("token") != null,
  msg: "",
  user: {
    id: loadId(),
    email: loadToken(),
    role: "",
  },
  isAdmin: false,
};

function loadToken() {
  const token = localStorage.getItem("token");
  if (token) {
    const payload: JWTPayload = jwt(token);
    return payload.email;
  }
  return "";
}
function loadId() {
  const token = localStorage.getItem("token");
  if (token) {
    const payload: JWTPayload = jwt(token);
    return payload.id;
  }
  return "";
}

export const authReducers = (
  state: IAuthState = initialState,
  action: IAuthAction
): IAuthState => {
  switch (action.type) {
    case "@@Auth/LOGIN":
      return {
        ...state,
        isAuthenticate: true,
      };
    case "@@Auth/LOAD_TOKEN":
      const token = action.token;
      const payload: JWTPayload = jwt(action.token);
      localStorage.setItem("token", token);
      const { email, role, id } = payload;
      if (role === "admin") {
        return {
          ...state,
          user: {
            email: email,
            role: role,
            id: id,
          },
          isAdmin: true,
        };
      } else {
        return {
          ...state,
          user: {
            email: email,
            role: role,
            id: id,
          },
          isAdmin: false,
        };
      }

    case "@@Auth/LOGOUT":
      localStorage.clear();
      return {
        ...state,
        isAuthenticate: false,
        user: {
          email: "",
          role: "",
          id: "",
        },
      };

    default:
      return state;
  }
};
