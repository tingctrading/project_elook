export function loadToken(token: string) {
  return {
    type: "@@Auth/LOAD_TOKEN" as const,
    token,
  };
}
export function loginSuccess() {
  return {
    type: "@@Auth/LOGIN" as const,
  };
}

export function logout() {
  return {
    type: "@@Auth/LOGOUT" as const,
  };
}

export function loadId(token: string) {
  return {
    type: "@@Auth/LOAD_TOKEN" as const,
    token,
  };
}

export type IAuthAction =
  | ReturnType<typeof loginSuccess>
  | ReturnType<typeof loadToken>
  | ReturnType<typeof logout>
  | ReturnType<typeof loadId>;
