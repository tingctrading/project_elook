import { IParkingState, ParkingItemState } from "./state";

export function GotPostedParkingItems(data: ParkingItemState[]) {
  return {
    type: "@@Parking/LOAD_POSTED_PARKING_ITEMS" as const,
    data,
  };
}

type FAILED_INTENT = "@@Parking/GOT_PARKING_ITEMS_FAILED";

export function failed(type: FAILED_INTENT, msg: string) {
  return {
    type,
    msg,
  };
}

export function SetMapInstance(data: any) {
  return {
    type: "@@Parking/SET_MAP_INSTANCE" as const,
    data,
  };
}
export function SetMapApi(data: any) {
  return {
    type: "@@Parking/SET_MAP_API" as const,
    data,
  };
}
export function SetMapApiLoaded() {
  return {
    type: "@@Parking/SET_MAP_API_LOADED" as const,
  };
}

export function UpdateParkingItemsGeometry(data: any) {
  return {
    type: "@@Parking/UPDATE_PARKING_ITEMS_GEOMETRY" as const,
    data,
  };
}

export function GotUserByUserId(email: any) {
  return {
    type: "@@PARKING/GOT_USER_BY_USER_ID" as const,
    email,
  };
}

export type IParkingAction =
  | ReturnType<typeof GotUserByUserId>
  | ReturnType<typeof GotPostedParkingItems>
  | ReturnType<typeof failed>
  | ReturnType<typeof SetMapInstance>
  | ReturnType<typeof SetMapApi>
  | ReturnType<typeof SetMapApiLoaded>
  | ReturnType<typeof UpdateParkingItemsGeometry>;
