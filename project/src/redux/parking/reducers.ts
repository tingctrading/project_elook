import { IParkingAction } from "./actions";
import { IParkingState, ParkingItemState } from "./state";

const initialState: IParkingState = {
  parkingItems: [],
  mapApi: null,
  mapApiLoaded: false,
  mapInstance: null,
};

export const parkingReducers = (
  state: IParkingState = initialState,
  action: IParkingAction
): any => {
  switch (action.type) {
    case "@@Parking/LOAD_POSTED_PARKING_ITEMS":
      const newParkingItems: ParkingItemState[] = [...action.data];
      return {
        ...state,
        parkingItems: newParkingItems,
      };
    case "@@Parking/SET_MAP_API":
      const newMapApi = { ...action.data };
      return {
        ...state,
        mapApi: newMapApi,
      };
    case "@@Parking/SET_MAP_API_LOADED":
      return {
        ...state,
        mapApiLoaded: true,
      };
    case "@@Parking/SET_MAP_INSTANCE":
      const newMapInstance = { ...action.data };
      return {
        ...state,
        mapInstance: newMapInstance,
      };
    case "@@Parking/UPDATE_PARKING_ITEMS_GEOMETRY":
      const newData = { ...action.data };
      return {
        ...state,
        parkingItems: newData,
      };

    case "@@PARKING/GOT_USER_BY_USER_ID":
      return {
        ...state,
      };

    default:
      return state;
  }
};
