import { IRepairAction } from "./actions";
import { IRepairState, RepairItemState } from "./state";

const initialState: IRepairState = {
  repairItems: [],
};

export const repairReducers = (
  state: IRepairState = initialState,
  action: IRepairAction
): any => {
  switch (action.type) {
    case "@@Repair/LOAD_REPAIR_ITEMS":
      const newRepairItems: RepairItemState[] = [...action.data];
      return {
        ...state,
        repairItems: newRepairItems,
      };

    default:
      return state;
  }
};
