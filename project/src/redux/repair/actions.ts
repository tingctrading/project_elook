import { IRepairState, RepairItemState } from "./state";

export function GotRepairItems(data: RepairItemState[]) {
  return {
    type: "@@Repair/LOAD_REPAIR_ITEMS" as const,
    data,
  };
}

type FAILED_INTENT = "@@Repair/GOT_REPAIR_ITEMS_FAILED";

export function failed(type: FAILED_INTENT, msg: string) {
  return {
    type,
    msg,
  };
}

export type IRepairAction =
  | ReturnType<typeof GotRepairItems>
  | ReturnType<typeof failed>;
