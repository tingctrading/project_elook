import { Dispatch } from "redux";
import { fetchGetRepairItem } from "../../api/repair";
import { failed, GotRepairItems, IRepairAction } from "./actions";
import { RepairItemState } from "./state";

export function getRepairItemThunk() {
  return async (dispatch: Dispatch<IRepairAction>) => {
    const res = await fetchGetRepairItem();
    const result: any = await res.json();
    const items: RepairItemState[] = result.items;

    if (res.ok) {
      dispatch(GotRepairItems(items));
    } else {
      dispatch(
        failed("@@Repair/GOT_REPAIR_ITEMS_FAILED", "fail getRepairItemThunk")
      );
    }
  };
}
