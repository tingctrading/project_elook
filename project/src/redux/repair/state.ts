export interface RepairItemState {
  id: number;
  name: string;
  location: string;
  phone: number;
  district: string;
  place_id: string;
  status: string;
}

export interface IRepairState {
  repairItems: RepairItemState[];
}
