import { IAdminAction } from "./actions";
import { IAdminState, ParkingItemState } from "./state";

const initialState: IAdminState = {
  parkingItems: [],
  pendingItems: [],
  selectedItemId: null,
};
export const adminReducers = (
  state: IAdminState = initialState,
  action: IAdminAction
): IAdminState => {
  switch (action.type) {
    case "@@Admin/LOAD_POSTED_PARKING_ITEMS":
      const newParkingItems: ParkingItemState[] = [...action.data];
      return {
        ...state,
        parkingItems: newParkingItems,
      };

    case "@@Admin/LOAD_PENDING_PARKING_ITEMS":
      const newPendingItems: ParkingItemState[] = [...action.data];
      return {
        ...state,
        pendingItems: newPendingItems,
      };
    case "@@Admin/GOT_USER_BY_USER_ID":
      return {
        ...state,
      };

    default:
      return state;
  }
};
