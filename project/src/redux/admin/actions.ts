import { ParkingItemState } from "./state";

export function GotPostedParkingItems(data: ParkingItemState[]) {
  return {
    type: "@@Admin/LOAD_POSTED_PARKING_ITEMS" as const,
    data,
  };
}

export function GotPendingParkingItems(data: ParkingItemState[]) {
  return {
    type: "@@Admin/LOAD_PENDING_PARKING_ITEMS" as const,
    data,
  };
}

export function GotUserByUserId(email: any) {
  return {
    type: "@@Admin/GOT_USER_BY_USER_ID" as const,
    email,
  };
}

type FAILED_INTENT = "@@Admin/GOT_PARKING_ITEMS_FAILED";

export function failed(type: FAILED_INTENT, msg: string) {
  return {
    type,
    msg,
  };
}

export type IAdminAction =
  | ReturnType<typeof GotUserByUserId>
  | ReturnType<typeof GotPostedParkingItems>
  | ReturnType<typeof GotPendingParkingItems>
  | ReturnType<typeof failed>;
