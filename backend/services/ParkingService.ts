import { Knex } from "knex";
import { ParkingItem, User } from "./models";

export class ParkingService {
  constructor(private knex: Knex) {}

  async getPosted() {
    const result = await this.knex<ParkingItem>("parking_place")
      .select("*")
      .where("post", true)
      .orderBy("id", "desc");
    return result;
  }

  async getById(id: number) {
    const result = await this.knex<ParkingItem>("parking_place")
      .select("id", "description", "is_checked")
      .where("user_id", id)
      .orderBy("id", "desc");
    return result;
  }

  async getUserById(id: number) {
    const user = await this.knex<User>("users").select("email").where({ id });
    return user;
  }
}
