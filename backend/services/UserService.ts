import { Knex } from "knex";

import { ParkingItem, User } from "./models";

export class UserService {
  constructor(private knex: Knex) {}

  async getUserByUsername(email: string) {
    const user = await this.knex
      .select("*")
      .from("users")
      .where({ email: email })
      .first();
    return user as User;
  }

  async getUserById(id: number) {
    const user = await this.knex<User>("users").where({ id }).first();
    return user;
  }

  async createUser(email: string, password: string) {
    const user = (
      await this.knex("users")
        .insert({ email, password, role: "member" })
        .returning("*")
    )[0];
    return user;
  }

  async changePassword(email: string, password: string) {
    const user = await this.knex("users").update({ password }).where({ email });
    return user;
  }

  async getReportedItems(id: number) {
    const result = await this.knex<ParkingItem>("parking_place")
      .select("*")
      .where("users_id", id)
      .orderBy("id", "desc");
    return result;
  }
}
