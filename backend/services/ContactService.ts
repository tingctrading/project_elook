import { Knex } from "knex";
import { ContactListItem } from "./models";

export class ContactListService {
  constructor(private knex: Knex) {}

  async getAll() {
    const result = await this.knex<ContactListItem>("car_towing")
      .select("*")
      .orderBy("id");
    return result;
  }
}
