import { Knex } from "knex";
import { hashPassword } from "../utils/hash";

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("parking_place").del();
  await knex("car_towing").del();
  await knex("car_repair").del();
  await knex("users").del();

  // Inserts seed entries
  const users_ids = await knex("users")
    .insert([
      {
        email: "ting@elook.com",
        role: "admin",
        password: await hashPassword("ting"),
      },
      {
        email: "keith@elook.com",
        role: "admin",
        password: await hashPassword("keith"),
      },
      {
        email: "quincy@elook.com",
        role: "admin",
        password: await hashPassword("quincy"),
      },
      {
        email: "member@elook.com",
        role: "member",
        password: await hashPassword("member"),
      },
    ])
    .returning("id");

  await knex("parking_place").insert([
    {
      place: "林士街停車場",
      location: "香港上環林士街2號",
      place_id: "ChIJH020Dn0ABDQRMgBZi_qhThQ",
      hourly_rent: "26",
      day_rent: "40",
      night_rent: "50",
      isIndoor: false,
      image: "",
      users_id: users_ids[0],
      post: true,
    },
    {
      place: "荃灣停車場",
      location: "荃灣青山公路-荃灣段174-208號",
      place_id: "ChIJddytKfD4AzQR0Bw5sUYnEiU",
      hourly_rent: "26",
      day_rent: "40",
      night_rent: "50",
      isIndoor: true,
      image: "",
      users_id: users_ids[0],
      post: true,
    },
    {
      place: "香港仔停車場",
      location: "香港仔香港仔水塘道18號",
      place_id: "ChIJqYJKUBsABDQRhRiZKMK3MAM",
      hourly_rent: "30",
      day_rent: "40",
      night_rent: "50",
      isIndoor: true,
      image: "",
      users_id: users_ids[1],
      post: true,
      remark: "風涼水冷",
    },
    {
      place: "葵芳停車場",
      location: "葵涌葵義路19號",
      place_id: "ChIJS4eqZ7z4AzQRh0VlKQWGPdU",
      hourly_rent: "25",
      day_rent: "40",
      night_rent: "50",
      isIndoor: true,
      image: "",
      users_id: users_ids[2],
      post: true,
      remark: "鄰近地鐵站",
    },
  ]);
  await knex("car_towing").insert([
    { name: "全記拖車", phone: "92892892", language: "中" },
    {
      name: "369電單車拖運服務",
      phone: "69269200",
      language: "中",
      second_language: "Eng",
    },
    { name: "富裕拖車", phone: "81061066", language: "中" },
    { name: "拖車易", phone: "62000300", language: "中" },
    { name: "731拖車服務", phone: "62102880", language: "中" },
    { name: "權記拖車", phone: "98071876", language: "中" },
    { name: "雄記拖車", phone: "98743330", language: "中" },
    { name: "有記", phone: "69808826", language: "中" },
    { name: "山本", phone: "94882598", language: "中", second_language: "Eng" },
    { name: "名驅", phone: "90889200", language: "中", second_language: "Eng" },
    { name: "至愛", phone: "94155070", language: "中" },
    {
      name: "SUNNY 拖車",
      phone: "65700009",
      language: "中",
      second_language: "Eng",
    },
    { name: "阿維", phone: "96189419", language: "中" },
    { name: "長城", phone: "55409394", language: "中" },
    { name: "森哥", phone: "55309394", language: "中" },
    { name: "V仔", phone: "92828335", language: "中" },
    { name: "電池俠", phone: "90366752", language: "中" },
    {
      name: "Ashley Roadside Assist",
      phone: "55040493",
      second_language: "Eng",
    },
    {
      name: "Lantau Island tow truck",
      phone: "26682999",
      second_language: "Eng",
    },
    { name: "Ibike", phone: "63827777", second_language: "Eng" },
    { name: "Fat Boy Bike Tow", phone: "590777730", second_language: "Eng" },
    { name: "Two Wheels", phone: "63030555", second_language: "Eng" },
  ]);
  await knex("car_repair").insert([
    {
      name: "一角工房",
      location: "土瓜灣燕安街28號",
      phone: "55485044",
      district: "九龍區",
      place_id: "ChIJFawBPpEBBDQRGCRSPOoXF5A",
    },

    {
      name: "MotoMall",
      location: "長沙灣廣成街4號康輝大廈B座地下22號舖",
      phone: "68413454",
      district: "九龍區",
      place_id: "ChIJA1PbllEHBDQR6EpfjksGlGc",
    },
    {
      name: "堅記車房",
      location: "柴灣連城道16號",
      phone: "66867666",
      district: "香港島",
      place_id: "ChIJXS8VXzABBDQRkdA_mNr98cE",
    },
    {
      name: "浩志電單車行",
      location: "鰂魚涌英皇道883號麗景樓Unit C G/F",
      phone: "94140930",
      district: "香港島",
      place_id: "ChIJJ9a65w0BBDQRxUJ3f8ciB3U",
    },
  ]);
}
