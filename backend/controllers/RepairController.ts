import { RepairService } from "../services/RepairService";
import { Request, Response } from "express";
import { logger } from "../utils/logger";

export class RepairController {
    private readonly tag = "RepairController";
    constructor(private repairService: RepairService) {}

    getAll = async (req: Request, res: Response) => {
        try {
          const items = await this.repairService.getAll();
          res.json({ items });
        } catch (err) {
          logger.error(`[${this.tag}] ${err.message}`);
          res.status(500).json({ message: "internal server error" });
        }
      };
}