import { ReportService } from "../services/ReportService";
import { Request, Response } from "express";
import { logger } from "../utils/logger";

export class ReportController {
  private readonly tag = "ReportController";
  constructor(private ReportService: ReportService) { }

  getAll = async (req: Request, res: Response) => {
    try {
      const items = await this.ReportService.getAll();
      res.json({ items });
    } catch (err) {
      logger.error(`[${this.tag}] ${err.message}`);
      res.status(500).json({ message: "internal server error" });
    }
  };

  createReportData = async (req: Request, res: Response) => {
    try {
      const reportData = req.body.data
      const items = await this.ReportService.createReportData(reportData);
      // console.log(items)
      res.json({ items });
    } catch (err) {
      logger.error(`[${this.tag}] ${err.message}`);
      res.status(500).json({ message: "internal server error" });
    }
  };
}