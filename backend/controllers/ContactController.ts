import { ContactListService } from "../services/ContactService";
import { Request, Response } from "express";
import { logger } from "../utils/logger";

export class ContactListController {
    private readonly tag = "ContactListController";
    constructor(private contactListService: ContactListService) { }

    getAll = async (req: Request, res: Response) => {
        try {
            const items = await this.contactListService.getAll();
            res.json({ items });
        } catch (err) {
            logger.error(`[${this.tag}] ${err.message}`);
            res.status(500).json({ message: "internal server error" });
        }
    };
}