import express from "express";
import { ContactListService } from "../services/ContactService";
import { ContactListController } from "../controllers/ContactController";
import { knex } from "../app";

const contactListService = new ContactListService(knex);
const contactListController = new ContactListController(contactListService);

export const contactListRoutes = express.Router();

contactListRoutes.get("/", contactListController.getAll); 