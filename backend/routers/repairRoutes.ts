import express from "express";
import { RepairService } from "../services/RepairService";
import { RepairController } from "../controllers/RepairController";
import { knex } from "../app";

const repairService = new RepairService(knex);
const repairController = new RepairController(repairService);

export const repairRoutes = express.Router();

repairRoutes.get("/", repairController.getAll); //localhost:8080/api/repair [GET]
