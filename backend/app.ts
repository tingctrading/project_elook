import express from "express";
import cors from "cors";
import Knex from "knex";
import * as knexConfig from "./knexfile";
export const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

const app = express();
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ limit: "50mb" }));

const API_VERSION = "/api";
import { routes } from "./routes";
import { logger } from "./utils/logger";
app.use(API_VERSION, routes); // localhost:8080/api

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  logger.info(`listening to PORT ${PORT}`);
});
